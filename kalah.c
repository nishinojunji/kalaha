#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MAX_V 999
#define MIN_V -999
#define DEPTH 4
#define SIZE 12

int board[14] = {};
int sumnum;

typedef struct value_move {
  int value;
  int move[12];
} value_move;

void start(){
  int stone = 1;
  for(int i=0; i<14; i++) {
    board[i] = stone;
  }
  board[6] = board[13] = 0;
  for(int i=0; i<14; i++) {
    sumnum += board[i];
  }
  return;
}

void print_board(){
  printf("\n");
  // Line 1
  printf("    ");
  for(int i=12; i>=7; i--) {
    printf("%2d ", board[i]);
  }
  printf("\n");
  // Line 2
  printf("    -6--5--4--3--2--1-\n");
  printf("%2d <P2--------------P1> %2d\n", board[13], board[6]);
  printf("    -1--2--3--4--5--6-\n");
  // Line 3
  printf("    ");
  for(int i=0; i<=5; i++) {
    printf("%2d ", board[i]);
  }
  printf("\n\n");
  return;
}

int result(){
  int sum1 = 0, sum2 = 0;
  for(int i=0; i<=5; i++) {
    sum1 += board[i];
  }
  for(int i=7; i<=12; i++) {
    sum2 += board[i];
  }
  if(board[6] > sumnum/2 || board[13] > sumnum/2){
    return 1;
  }
  return 0;
}

int sowing(int number, int player){
  int stone = board[number];
  board[number] = 0;
  while(stone > 0) {
    number++;
    if(number == 14) {
      number = 0;
    }
    if(number != 7*player-1) {
      board[number]++;
      stone--;
    }
  }

  printf("sowing : \n");
  print_board();
  printf("end sowing \n");

  return number;
}

void both_get(int number, int player){
  if(board[number] == 1 && board[12-number] != 0) {
    if((player == 0 && number <= 5 && number != 6) || (player == 1 && number >= 7 && number != 13)) {
      if(player == 0) {
        board[6] += (board[number] + board[12-number]);
      } else {
        board[13] += (board[number] + board[12-number]);
      }
      board[number] = board[12-number] = 0;
    }
  }
  return;
}

value_move move_first(int, int*);
value_move move_second(int, int*);

// ここからMiniMax

int eval_value(){
  return board[6] - board[13];
}

value_move move_first(int depth, int *board){
  value_move vm;
  vm.value = MIN_V;
  for(int i=0; i<SIZE; i++){
    vm.move[i] = -1;
  }
  if(depth == 0) {
    vm.value = eval_value();
    return vm;
  }
  for(int pos=0; pos<=5; pos++) {
    if(board[pos] == 0) {
      continue;
    }
    int b_copy[14];
    for(int i=0; i<14; i++) {
      b_copy[i] = board[i];
    }
    int kari_res = sowing(pos, 0);
    value_move kari;
    if(result() == 1) {
      vm.value = eval_value();
    } else if(kari_res == 6) {
      kari = move_first(depth, b_copy);
    } else {
      kari = move_second(depth-1, b_copy);
    }
    if(vm.value < kari.value) {
      vm.value = kari.value;
      for(int i=0; i < SIZE-1; i++){
        vm.move[i+1] = vm.move[i];
      }
      vm.move[0] = pos;
    }
  }
  return vm;
}

value_move move_second(int depth, int *board){
  value_move vm;
  vm.value = MAX_V;
  for(int i=0; i<SIZE; i++){
    vm.move[i] = -1;
  }
  if(depth == 0) {
    vm.value = eval_value();
    return vm;
  }
  for(int pos=7; pos<=13; pos++) {
    if(board[pos] == 0) {
      continue;
    }
    int b_copy[14];
    for(int i=0; i<14; i++) {
      b_copy[i] = board[i];
    }
    int kari_res = sowing(pos, 0);
    value_move kari;
    if(result() == 1) {
      vm.value = eval_value();
    } else if(kari_res == 13) {
      kari = move_second(depth, b_copy);
    } else {
      kari = move_first(depth-1, b_copy);
    }
    if(vm.value > kari.value) {
      vm.value = kari.value;
      for(int i=0; i < SIZE-1; i++){
        vm.move[i+1] = vm.move[i];
      }
      vm.move[0] = pos;
    }
  }
  return vm;
}

// ここまでMiniMax

void play_game(){
  int player=0;
  while(result() == 0) {
    player %= 2;
    value_move vm;
    if(player == 0) {
      vm = move_first(DEPTH, board);
    } else {
      vm = move_second(DEPTH, board);
    }
    for(int i=0; vm.move[i] >= 0; i++) {
      printf("Move %2d\n", vm.move[i]);
      sowing(vm.move[i], player);
      print_board();
    }
    player++;
  }
  return;
}

void final_result(){
  if(result() == 1) {
    for(int i=7; i<=12; i++) {
      board[13] += board[i];
      board[i] = 0;
    }
  } else {
    for(int i=0; i<=5; i++) {
      board[6] += board[i];
      board[i] = 0;
    }
  }
  printf("It's over!\n");
  print_board();
  printf("P1 %2d VS %2d P2\n", board[6], board[13]);
  if(board[6] == board[13]) {
    printf("Result: Draw!\n");
  } else if(board[6] > board[13]) {
    printf("Player 1 WIN!\n");
  } else {
    printf("Player 2 WIN!\n");
  }
  return;
}

int main(void){
  start();
  play_game();
  final_result();
  return 0;
}
